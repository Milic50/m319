# Kompetenzenraster

Das nachfolgende Kompetenzenraster zeigt die relevanten Kompetenzen, welche die Lernenden im Modul 319 am Gewerblich-industriellen Bildungszentrum Zug erreichen können. Diese Kompetenzen sind in vier verschiedene Kompetenzbereiche (horizontal) mit jeweils vier unterschiedlichen Kompetenzstufen (vertikal) gegliedert.

|                                                 |                         | Beginner                                                     | Intermediate                                                 | Advanced                                                     | Expert                                                       |
| :---------------------------------------------- | ----------------------- | :----------------------------------------------------------- | :----------------------------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| ① **Probleme analysieren**                      |                         | Kann Anforderungen aufnehmen und dokumentieren.              | Kann Problemraum und Lösungsraum gezielt unterscheiden.      | Kennt Instrumente zur lösungsfreien Identifikation von Anforderungen. | Kennt Gründe für die Trennung von Problem- und Lösungsraum.  |
| ② **Lösungen darstellen**                       |                         | Kennt den Aufbau und wesentliche Elemente eines Struktogramm. | Kann ein Struktogramm zur Dokumentation eines vorgegebenen Lösungsansatzes einsetzen. | Kann ein Struktogramm zur Dokumentation eines selber entwickelten Lösungsansatzes einsetzen. | Kann Struktogramme als Grundlage für eine Besprechung bzw. Diskussion verwenden. |
| ③ **Lösungen umsetzen**                         | ⓐ Variablen, Konstanten | Kann Variablen und Konstanten deklarieren und definieren.    | Kennt Nutzen und Unterschied von Variablen und Konstanten.   | Kann den Gültigkeitsbereich von Variablen und Konstanten gezielt einsetzen. | Wendet Regeln und Konventionen bei der Namensgebung korrekt an. |
|                                                 | ⓑ Datentypen            | Kennt wesentliche, primitive Datentypen.                     | Kennt einige wesentliche, komplexe Datentypen.               | Kann eindimensionale Arrays anwenden.                        | Kann die implizite und explizite Typumwandlung anwenden und kennt deren Auswirkungen. |
|                                                 | ⓒ Selektion             | Kann bedingte Anweisungen mit `if` korrekt anwenden.         | Kann bedingte Anweisungen mit alternativen Ausführungspfaden mit `if` und `else` korrekt anwenden. | Kann bedingte Anweisungen mit mehr als zwei Ausführungspfaden im `switch`-Konstrukt korrekt anwenden. | Kann den ternären bedingten Operator (`?:`) korrekt anwenden. |
|                                                 | ⓓ Iteration             | Kann Anweisungen mit der `while`- und `do-while`-Schleife wiederholt ausführen. | Kann Anweisungen mit der `for`-Schleife wiederholt ausführen. | Kann die `while`- und `for`-Schleife situationsbedingt und gezielt einsetzen. | Kann verschachtelte Schleifen zur Lösung anspruchsvoller Probleme korrekt einsetzen. |
|                                                 | ⓔ Funktionen            | Kennt die Syntax zur Definition und für den Aufruf einer Funktion. | Kann Funktionen mit und ohne Rückgabewert gezielt einsetzen. | Kann Funktionen mit mehreren und unterschiedlichen Parametern sinnvoll implementieren und aufrufen. | Kann den Mehrwert für die Auslagerung von Codefragmenten in eigene Funktionen nachvollziehbar erklären. |
| ④ **Fehler vermeiden, erkennen und beseitigen** |                         | Kann Kommentare zur Erhöhung der Verständlichkeit im Quellcode gezielt einsetzen. | Kann den Debugger in einer IDE korrekt und gewinnbringend einsetzen. | Kennt relevante Konventionen bei der Programmierung und kann diese konsequent anwenden. | Kennt Gründe und Ziele für das Testen von Software.          |

## Kombetenzbereiche

Die Kompetenzbereiche unterteilen die Gesamtmenge aller Kompetenzen in vier inhaltliche Themenfelder. Innerhalb dieser Themenfelder sind die Kompetenzen aufsteigend in vier Kompetenzstufen angeordnet.

### Probleme analysieren

Applikationsentwicklerinnen und Applikationsentwickler schreiben Software um Aufgaben und Prozesse ihrer Auftraggeberinnen und Auftraggeber zu ermöglichen bzw. zu vereinfachen. Damit die Erstellung von Software erfolgreich sein kann, muss das Problem zuvor analysiert und verstanden werden.

### Lösungen darstellen

Ein wesentlicher Bestandteil von Software sind Prozesse, Abläufe und Algorithmen. Diese können und sollen aus verschiedenen Gründen grafisch dargestellt werden. Einerseits kann mit solchen Darstellungen die bereits implementierte Lösung dokumentiert werden und andererseits kann die Darstellung vor der Implementation genutzt werden um den Prozess der Programmierung schrittweise zu vereinfach oder um verschiedene Lösungsmöglichkeiten zu vergleichen.

### Lösungen umsetzen

Ein weiterer, wesentlicher Teil der Softwareentwicklung ist die Programmierung. Dazu sind Kenntnisse und Anwendungskompetenzen mindestens einer Programmiersprache in verschiedenen Bereichen erforderlich. Der Kompetenzbereich *Lösungen umsetzen* ist daher in fünf Unterbereiche unterteilt. Jeder dieser vier Unterbereiche ⓐ bis ⓔ entspricht einem essentiellen Teil jeder Programmiersprache und gehört daher zu den grundlegenden Werkzeugen jeder Applikationsentwicklerin und jedes Applikationsentwicklers.

### Fehler vermeiden, erkennen und beseitigen

Die Vermeidung und Reduktion von Fehlern ist ein wesentlicher Qualitätsaspekt von Software. In diesem Kompetenzbereich geht es daher um Arbeitsmethodiken zum Vermeiden, Erkennen und Beseitigen von Fehlern. Dazu gehören neben ausgewählten Grundlagen von *Clean Code* auch die Handhabung des Debuggers in einer IDE (IDE = *Integrated Development Environment*, integrierte Entwicklungsumgebung) sowie grundlegende Kenntnisse über Testing.

## Kompetenzstufen

Die einzelnen Kompetenzen innerhalb eines Kompetenzbereichs sind in vier Kompetenzstufen mit zunehmender Komplexität unterteilt.

### Beginner

Die Kompetenzen der Stufe *Beginner* gehören zu den absoluten Grundlagen und bilden damit die Basis für alle weiteren Kompetenzen. 

### Intermediate

Die Kompetenzen der Stufe *Intermediate* bauen meist direkt auf den Grundlagen der vorhergehenden Stufe auf und stellen gewissermassen "erweiterte Grundlagen" dar. Sie sind essentiell um die täglichen Herausforderungen des beruflichen Alltages effektiv und effizient zu bewältigen.

### Advanced

Die Kompetenzen auf der Stufe *Advanced* vertiefen die zuvor erworbenen Kompetenzen auf den darunterliegenden Kompetenzstufen und erweiteren die Fähigkeiten einer Applikationsentwicklerin bzw. eines Applikationsentwicklers um relevante Aspekte.

### Expert

Die Kompetenzen auf der Stufe *Expert* komplettieren das fachliche und methodische Repertoire einer kompetenten Applikationsentwicklerin und eines kompetenten Applikationsentwicklers. Sie sind häufig mit Reflexion der zuvor erworbenen Kompetenzen und der gezielten Anwendung, Abwägung oder Adaption von grundlegenden Kompetenzen verbunden und erfordern daher fortgeschrittene Programmierkenntnisse.