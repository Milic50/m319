# Struktogramm

Struktogramme, auch *Nassi-Schneiderman-Diagramme* genannt, werden in der Informatik häufig eingesetzt, um Algorithmen und Programmabläufe darzustellen. Der Namen *Struktogramm* weist auf die zentralen Aspekte beim Einsatz von Struktogrammen hin: Programmstrukturen und Kontrollstrukturen.

## Unabhängigkeit von der Programmiersprache

Probleme und Algorithmen können meist in vielen verschiedenen Programmiersprachen implementiert werden. Die grundlegende Logik bleibt dabei meist unverändert. Lediglich die Syntax oder die sprachspezifischen Schlüsselwörter (*Keywords*) sind in jeder Programmiersprache leicht unterschiedlich. Durch die Darstellung eines Algorithmus mit einem Struktogramm kann die Abfolge der einzelnen Anweisungen und Kontrollstrukturen auf neutrale und damit allgemein verständliche Art abgebildet werden. Eine versierte C#-Programmiererin kann also die Implementation Ihres Algorithmus mit der Programmiersprache C# als Struktogramm abbilden. Dieses Struktogramm kann dann auch von anderen Programmierern beispielsweise mit *Java*, *PHP*, *Pyhon*, *Swift* oder vielen anderen Programmiersprachen umgesetzt werden. Umgekehrt funktioniert das natürlich genauso.

## Darstellung

Struktogramme sind eine strukturierte, graphische Darstellungsform von Algorithmen. Mit dieser Darstellung ist es möglich, den genauen Ablauf mit allen möglichen Verzweitungen und Varianten detailliert und fachlich korrekt zu beschreiben. Dabei ist das fertige Struktogramm immer eine Rechteck, welches im Inneren verschiedene Blöcke enthält. Die einzelnen Blöcke sehen unterschiedlich aus und haben folglich eine jeweils unterschiedliche, klar definierte Bedeutung.

Neben der graphischen Darstellung der einzelnen Blöcke, werden spezifische Informationen (Daten, Bedingungen, Anweisungen, ...) in textueller Form ins Innere der Blöcke geschrieben. Dabei ist auf eine allgemein verständliche, präzise und sprachagnostische (d.h. von der Programmiersprache unabhängige) Formulierung zu achten.

Die Ausführung des Algorithmus erfolgt "von oben nach unten". Bei der Analyse bzw. der Implementation eines Struktugramms in einer beliebigen Programmiersprache, wird folglich mit dem ersten Block innerhalb des Struktogramm-Rechtecks begonnen. Die weitere Ausführung des Algorithmus ist abhängig von den jeweils vorhergehenden Anweisungen und Daten. Dabei kann es aufgrund der Strukturierung der verschiedenen Blöcke möglich sein, dass einzelne Blöcke übersprungen (d.h. nicht ausgeführt) oder wiederholt ausgeführt werden.

Die nachfolgenden Kategorien beschreiben einige Gruppen von Blöcken.

### Anweisungen

Die Anweisung ist ein einfacher, einzelner Schritt innerhalb der Algorithmus. Anweisungen werden alternativ auch als *Sequenz*, *Befehl* oder *Folge* bezeichnet. Mehrere Anweisungen bilden eine *Anweisungsfolge*, einen *linearen Ablauf* oder eine *Befehlsfolge*.

### Selektion

Eine Selektion wird genutzt um unterschiedliche Ausführungsstränge innerhalb eines Algorithmus abzubilden. Typischerweise wird bei einer Selektion genau eine mögliche Ausführungsvariante ausgeführt. Einige Programmiersprachen kennen auch Möglichkeiten zur sequentiellen Ausführung mehrerer Ausführungsvarianten. Dies kann mit der gängigen Notation in Struktogrammen jedoch nicht direkt abgebildet werden.

Bei der Selektion werden verschiedene Variaten unterschieden. Es gibt Selektionsblöcke mit lediglich einer möglichen Ausführungsvariante. Dabei wird diese Ausführungsvariante - abhängig von der Evaluation der Bedingung - entweder ausgeführt oder nicht ausgeführt. Bei zwei oder mehreren möglichen Ausführungsvariaten wird das Ergebnis der Bedingung dazu verwendet, die relevante Ausführungsvariante zu bestimmen. In einer Variante des Selektionsblockes kann zudem eine Ausführungsvariante spezifiziert werden, welche ausgeführt wird, wenn das Ergebnis der Bedingung auf keine der anderen Ausführungsvarianten zutrifft.

Die Selektion wird alternativ auch als *Verzweigung* oder *Alternative* bezeichnet.

### Iteration

Iterationsblöcke werden genutzt um die wiederholte Ausführung einer Anweisungsfolge zu notieren. Dabei werden alle Blöcke im Inneren der Iteration potentiell mehrfach ausgeführt. Die Anzahl der Wiederholungen wird durch eine numerische Angabe oder durch die Fomulierung einer Bedingung im *Kopf* oder *Fuss* der Schleife spezifiziert. Die Iterationsblöcke werden aufgrund des Zeitpunktes, zu welchem die Bedingungsprüfung erfolgt, in Iterationen mit *vorausgehender* und *nachfolgender* Bedingungsprüfung unterschieden. Diese Unterscheidung ist in der graphischen Darstellung erkennbar.

### Weitere Blöcke

Neben den zuvor genannten Gruppen für *Anweisung*, *Selektion* und *Iteration* existieren verschiedene weitere Blöcke. Beispielsweise können spezifische Blöcke für die vorzeitige Beendigung eines Algorithmus oder für die Ausführung eines *Unterprogramms* (*Prozedur*, *Funktion* oder *Methode*) genutzt werden.

## Verwendung

Mit einem Struktogramm wird ein Algorithmus formal und unabhängig von einer Programmiersprache dargestellt. Grundsätzlich können Struktogramme auch ausserhalb der Programmierung genutzt werden. Im Bereich der Softwareentwicklung werden Struktogramme zur *Dokumentation* eines bereits implementierten Algorithmus oder aber zur *Konzeption* einer später folgenden Implementation eingesetzt. Im ersten Fall (*Dokumentation*) wird mit dem Struktogramm eine Schlüsselstelle oder ein besonders komplexer bzw. wichtiger Aspekt der Software detailliert und formalisiert dargestellt. Dies hilft anderen Programmiererinnen und Programmierern beim Verständnis des geschriebenen Programmcodes. Im zweiten Fall (*Konzeption*) wird das Struktogramm vor der eigentlichen Implementation des Algorithmus in Code erstellt. Dabei geht es um das Aufzeigen bzw. Vorgeben der nachfolgenden Implementation. So können Softwareentwicklerinnen und -entwickler beispielsweise über verschiedene Varianten der Implementation diskutieren.

### Verwendung in der Praxis

Es soll dabei nicht verschwiegen werden, dass in der Praxis Struktogramme relativ selten zur *Dokumentation* oder *Konzeption* von Software eingesetzt werden. Die damit erreichte Grad an Detaillierung ist häufig nicht erforderlich oder gewünscht. Stattdessen werden häufig andere Darstellungsformen wie *Aktivitätsdiagramme* oder *Sequenzdiagrame* zur Darstellung von Code auf einer höheren Abstraktionsebene eingesetzt.

### Verwendung in der Ausbildung

Bei Programmieranfängerinnen und -anfängern, beim Erlernen einer Programmiersprache und in der Ausbildung (Schule, Studium) werden Struktogramme hingegen relativ häfufig eingesetzt, weil damit der Aufbau und die Gliederung eines Algorithmus sprachagnostisch geübt werden kann (syntaktische Regeln einer Programmiersprache sind dabei irrelevant). Gleichzeitig kann durch Struktogramme der komplexe Vorgang zur Implementation eines Algorithmus in Programmcode in zwei einfachere Teilprobleme gegliedert werden. In einem ersten Schritt wird durch die Erstellung eines Struktogramms gewissermassen die grundlegende Struktur des Algorithmus abgebildet. Danach kann in einem zweiten Schritt dieses Struktogramm relativ einfach in Programmcode (einer beliebigen, strukturierten Programmiersprache) umgesetzt werden.

## Werkzeuge

Grundsätzlich können Struktogramme mit Stift und Papier (bzw. Stift und Touchscreen) erstellt werden. Das "freihändige" Erstellen von Struktogrammen birgt jedoch einige Hürden, inbesondere in der sauberen Darstellung. Deshalt werden Struktogramme vorzugsweise mit spezifischen Tools (Software) erstellt. Nachfolgend sind einige solcher Tools aufgeführt.

### Online-Tool der TU Dresden

Mit dem Online-Tool [Struktog](https://dditools.inf.tu-dresden.de/dev/struktog/) der TU Dresden können Struktogramme ein einem Browser erstellt werden. Die verschiedenen Block-Kategorien werden farblich unterschieden und die einzelnen Blöcke in einer übersichtlichen "Bibliothek" abgebildet. Struktogramme können als JSON-Datei oder als Bild exportiert werden. Zuvor erstellte Struktogramme können als JSON-Datei importiert werden. Zusätzlich kann für Struktogramme der jeweils passende Programmcode in drei verschiedenen Programmiersprachen (Java, Python, PHP) dargestellt werden.

### Structorizer

Das Tool [Structorizer](https://www.structorizer.com) ist ebenfalls ein Online-Werkzeug, welches ohne Installation direkt im Browser genutzt werden kann. Es wurde von Janis Häuser, einem ehemaligen Informatiklernenden (Systemtechnik) an der Gewerblich-industriellen Berufsschule Solothurn entwickelt. Mit dem *Structorizer* können Struktogramme erstellt und in verschiedenen Formaten exportiert werden. Die Bedienung ist etwas umständlicher als beim vergleichbaren Werkzeug der TU Dresden.

### Struktogrammer

Der [Struktogrammer](http://www.struktogrammer.ch/) ist eine Programm, welches heruntergeladen und auf dem eigenen Gerät installiert werden kann. Somit erfordert es keine aktive Internetverbindung für die Erstellung von Struktogrammen. Entwickelt wurde dieses Tool von Hansueli Steck, einem ehemaligen Lehrer am Gewerblich-industriellen Bildungszentrum Zug.

Der Struktogrammer verfügt über alle notwendigen Funktionen für die Erstellung von Struktogrammen sowie für das Speichern, Laden und Exportieren von Struktogrammen. Auf der Webseite ist zudem eine umfangreiche Anleitung zu finden.

