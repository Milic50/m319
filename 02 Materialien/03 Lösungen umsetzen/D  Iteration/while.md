# `while`-Iteration

Bei der `while`-Iteration werden Anweisungen so lange wiederholt, wie eine *Bedingung* erfüllt ist. Sobald die Bedingung *nicht mehr erfüllt* ist, wird die Ausführung der Schleife abgebrochen. Dabei wird die Bedingung vor jeder Ausführung des Blocks (Code, der zwischen `{` und `}` steht) überprüft. Die Anweisungen im Schleifenkörper werden im Extremfall *nie* ausgeführt.

```c#
while (Bedingung)
{
    Anweisungen
}
```

Die Bedingung wird nach dem `while` Schlüsselwort zwischen runde Klammern geschrieben. Sie ist ein Ausdruck, welcher zu einem boolschen Wert (`true` oder `false`) evaluieren muss. Typischerweise wird im Inneren der `while`-Schleife eine Variable so verändert, dass die Bedingung in Abhängigkeit dieser Variable entweder *erfüllt* oder *nicht erfüllt* ist.

Wenn der zu wiederholende Code-Block aus nur einer einzigen Anweisung besteht, kann diese theoretisch ohne die geschweiften Klammern, direkt auf der Zeile unterhalb der Bedingung geschrieben werden. Von dieser Notation wird jedoch abgeraten, da sie gerade bei Programmieranfängerinnen und -anfängern schneller zu unübersichtlichen Code-Stellen oder Fehlern führen kann.

```c#
// This is necessary for the Console.WriteLine statement
// at the end of this code snippet.
using System;

// This is an imaginary, secret number.
int secretNumber = 42;

// In this variable, the computers guess of the
// secret number is kept.
int guess = 0;

// While loop to "guess" the secret number. It is repeated
// as long as the guess does not match the secretNumber.
while (guess != secretNumber)
{
    guess++;
}

Console.WriteLine($"I got it! The secret number is: {secretNumber}");
```
