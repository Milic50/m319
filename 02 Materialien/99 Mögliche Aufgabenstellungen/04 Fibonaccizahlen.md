# Fibonaccizahlen berechnen

> Die Fibonacci-Folge ist die unendliche Folge natürlicher Zahlen, die (ursprünglich) mit zweimal der Zahl 1 beginnt oder (häufig, in moderner Schreibweise) zusätzlich mit einer führenden Zahl 0 versehen ist.[1] Im Anschluss ergibt jeweils die Summe zweier aufeinanderfolgender Zahlen die unmittelbar danach folgende Zahl
> *Quelle: [Wikipedia](https://de.wikipedia.org/wiki/Fibonacci-Folge)*

Die Fibonacci-Folge beginnt so: `0, 1, 1, 2, 3, 5, 8, 13, 21, ...`. Die Zahl `0` am Anfang dieser Folge ist dabei optional und hat keine relevante Bedeutung.

Erstellen Sie ein Programm, welches die Fibonaccizahl an einer beliebigen Stelle innerhalb der Fibonacci-Folge berechnen kann.

## Beispiele

Bei den nachfolgenden Beispielen wird angenommen, dass die (optionale) Zahl `0` zu Beginn der Fibonacci-Folge der Position `0` entspricht. Die beiden Zahlen `1` folgen entsprechend an den Positionen `1` und `2`.

- Fibonaccizahl an der 5. Stelle: `5`
- Fibonaccizahl an der 12. Stelle: `144`
- Fibonaccizahl an der 42. Stelle: `267'914'296`

*Wie Sie sehen, wächst die Fibonacci-Folge relativ schnell an. Überfordern Sie Ihr Gerät also nicht mit der Berechnung von allzuhohen Fibonaccizahlen.*