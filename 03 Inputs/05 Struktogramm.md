# Input: Struktogramm

Für die [Aufgabe 6](../02%20Materialien/99%20Mögliche%20Aufgabenstellungen/06%20Spiegelzahl.md) wurde im Rahmen dieses Inputs ein Struktogramm erstellt. Für die Erstellung des Struktogramms wurde das Online Tool [Struktog.](https://dditools.inf.tu-dresden.de/dev/struktog/) der TU Dresden verwendet.

![Struktogramm für die Aufgabe 06: Spiegelzahl](Ressourcen/05_Struktogramm.png)

Das obenstehende Struktogramm kann durch Importieren der entsprechenden [JSON-Datei](Ressourcen/05_Struktogramm.json) im Online Tool [Struktog.](https://dditools.inf.tu-dresden.de/dev/struktog/) der TU Dresden editiert werden.