# Input: Live Coding

Basierend auf dem Struktogramm aus Input 5 wurde eine mögliche Lösung für die Aufgabe [06 Spiegelzahl](../02%20Materialien/99%20Mögliche%20Aufgabenstellungen/06%20Spiegelzahl.md) in C# implementiert:

```c#
using System;

Console.Write("Geben Sie eine mehrstellige, natürliche Zahl ein: ");
int inputNumber;

// Read an input from the console until there's a valid number.
do
{
    // Read user input from console.
    string original = Console.ReadLine();
Die
    // Check if the string input may be converted to a positive integer.
    if (!int.TryParse(original, out inputNumber) || inputNumber < 0)
    {
        Console.WriteLine("\nDer eingegebene Wert ist ungültig.");
        Console.Write("Geben Sie bitte eine positive, ganze Zahl ein: ");
    }

    // Check if the user input does not end with a zero.
    if (inputNumber % 10 == 0)
    {
        Console.WriteLine("\nEin Vielfaches der Zahl 10 kann nicht gespiegelt werden.");
        Console.Write("Geben Sie bitte eine andere Zahl ein: ");
    }
} while (inputNumber % 10 == 0 || inputNumber < 0);

int mirrorNumber = 0;

// Repeat for each digit...
for (int i = (int)Math.Ceiling(Math.Log10(inputNumber)); i > 0; i--)
{
    int digit = inputNumber % 10;
    inputNumber /= 10;
    mirrorNumber = 10 * mirrorNumber + digit;
}

Console.WriteLine($"\nDie Spiegelzahl lautet: {mirrorNumber}\n");
```