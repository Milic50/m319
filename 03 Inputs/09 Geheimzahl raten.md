# Input: Geheimzahl raten (Live Coding)

Für die [Aufgabe 2](../02%20Materialien/99%20Mögliche%20Aufgabenstellungen/02%20Geheimzahl%20raten.md) wird im Rahmen dieses Inputs eine mögliche Lösung erarbeitet.

```c#
using System;

// Define the upper boundary for the generation of secret numbers.
const int randomNumberRange = 50;

// Generate random number (for the player to guess)
Random randomNumberGenerator = new Random();
int randomNumber = randomNumberGenerator.Next(1, 1 + randomNumberRange);

// Variable must be declared here (outside of do-while loop)
// to be used within the loops condition.
int userInput;

do
{
    userInput = ReadUserInput();
    string output;

    if (userInput == randomNumber)
    {
        output = $"\nGRATULATION! Die gesuchte Geheimzahl ist {userInput}.\n";
    }
    else
    {
        output = userInput < randomNumber
            ? $"\nZU KLEIN! Die Geheimzahl ist grösser als {userInput}.\n"
            : $"\nZU GROSS! Die Geheimzahl ist kleiner als {userInput}.\n";
    }

    Console.WriteLine(output);

} while (userInput != randomNumber);

// This functions asks the user to enter a number to guess the secret, random number.
// The input (string) is convertet to an integer value, validated and returned.
int ReadUserInput()
{
    int userInput;

    do
    {
        Console.Write($"Erraten Sie die Geheimzahl zwischen 1 und {randomNumberRange}: ");
        string input = Console.ReadLine();

        if (!int.TryParse(input, out userInput) || !IsValueInValidRange(userInput))
        {
            Console.WriteLine("Der eingegebene Wert ist ungültig.");
        }
    } while (!IsValueInValidRange(userInput));

    return userInput;
}

// This function checks whether the given value is within the valid range for secret numbers.
bool IsValueInValidRange(int value)
{
    return value >= 1 && value <= randomNumberRange;
}
```